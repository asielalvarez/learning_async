﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Learning_Async
{
    class Program
    {
        static void Main(string[] args)
        {
            // Start stop watch
            Stopwatch sw = new Stopwatch();
            sw.Start();

            int count = 100000;

            //var result = runProgram(count);                      /* when count = 100000, it takes the program ~16 seconds to run */
            var result = runProgram_parallel_async(count);     /* when count = 100000, it takes the program ~4 seconds to run */

            // Stop stop watch
            sw.Stop();
            Console.WriteLine("Elapsed={0}", sw.Elapsed);
        }

        static List<int> runProgram(int count)
        {
            List<int> list = new List<int>();
            for (int i = 0; i < count; i++)
            {
                list.Add(wasteTime(count));
            }

            return list;
        }

        static async Task<int[]> runProgram_parallel_async(int count)
        {
            // create list of tasks
            List<Task<int>> tasks = new List<Task<int>>();

            // call waste time and add the task to the list
            for (int i = 0; i < count; i++)
            {
                tasks.Add(Task.Run(() => wasteTime(count)));
            }

            // get result when all tasks are done
            var result = Task.WhenAll(tasks);

            return result.Result;
        }

        static int wasteTime(int count)
        {
            for (int i = 0; i < count; i++)
            {                    
            }

            return 0;
        }

    }
}
